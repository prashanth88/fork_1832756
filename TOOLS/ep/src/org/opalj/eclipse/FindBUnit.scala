/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2015
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.eclipse

import java.io.File
import org.eclipse.core.runtime.IPath
import org.eclipse.jdt.core.IPackageFragmentRoot
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.resources.IWorkspaceRoot
import org.eclipse.core.resources.IProject
import org.eclipse.jdt.core.JavaCore
import org.eclipse.jdt.core.IJavaProject
import org.eclipse.core.runtime.NullProgressMonitor
import org.opalj.da.ClassFileReader.ClassFile
import org.opalj.da.ClassFileReader.AllClassFiles

/**
 * Helpers to find the bytecode unit given the name
 *
 * @author Prashanth Sadanand
 */
object FindBUnit {
    /**
     * Helper to find the bytecode for a unit with a given the name inside the packages of a project
     */
    def findClassInJars(jProject: IJavaProject, name: String): ClassFile = {
        var classFile: ClassFile = null
        val jars: List[IPackageFragmentRoot] = jProject.getPackageFragmentRoots().toList
        jars.filter{j => j.getKind.equals(IPackageFragmentRoot.K_BINARY) && j.isArchive()}
        //select all archives in the java project
              
        for (jar: IPackageFragmentRoot <- jars
                if classFile == null) {
            jar.open(new NullProgressMonitor)
            val jarPath: String = jar.getPath.toOSString()
            try{
                classFile = ClassFile(jarPath, name.replace(".", "/") + ".class").head
            } catch {
              case e: Exception => println("exception caught: " + e);
            }
        }
        classFile
    }
    
    def getClassPaths(project: IJavaProject, pathInPackage: String): List[IPath] = {
        val defaultPath = project.getOutputLocation.removeFirstSegments(1)
        val projectPaths = project.getRawClasspath.toList.map { _.getOutputLocation }
        val validProjectPaths = projectPaths.filter { _ != null }
        val relativeProjectPaths = validProjectPaths.map { _.removeFirstSegments(1) }.distinct
        val workspacePath = project.getProject.getLocation
        val binaryPaths = if (relativeProjectPaths.length == 0) {
            List(workspacePath.append(defaultPath))
        } else {
            relativeProjectPaths.map { e ⇒ workspacePath.append(e) }
        }
        binaryPaths.map { path ⇒ path.append(pathInPackage) }
    }
    
    /**
     * Helper to find the bytecode for a unit with a given the name inside the output folder of a given project
     */
    def findClassInSource(jProject: IJavaProject, name: String): ClassFile = {
        var classFile: ClassFile = null
        val n = name.lastIndexOf(".")
         
        val packageName: String = if(n < 0) "" else name.substring(0, n)
        packageName.replace(".", "/")
         
        val unitName: String = name.substring(n + 1)
        
        //search for binary unit in classpaths
        val absolutePaths: List[IPath] = getClassPaths(jProject, packageName)          
        def hasClass(e: IPath): Boolean =
            e.toFile.exists && e.toFile.list.contains(unitName+".class")
        val validPaths: List[IPath] = absolutePaths.filter(hasClass)
        
        def toRelatedFiles(e: IPath): List[File] =
            e.toFile.listFiles.filter(isRelated).toList
        def isRelated(e: File): Boolean =
            e.getAbsolutePath.matches(""".*(/|\\)"""+unitName+"""(\$.*)?\.class""")
        val relatedFiles: List[File] = validPaths.map(toRelatedFiles).flatten
        
        try{
            val classFiles: List[ClassFile] = AllClassFiles(relatedFiles).map(_._1).toList
            classFiles.length match {
                case n if n > 0 ⇒
                    classFile = classFiles.head //return matched class
            }
        } catch {
          case e: Exception => println("exception caught: " + e);
        }
  
        classFile
    }
    
    /**
     * Helper to find the bytecode for a unit with a given the name inside the active project
     * return the ClassFile representation found first
     */
    def unitLookup(name: String, activeProject: String): ClassFile = {
         var result: ClassFile = null
         
         val workspaceRoot: IWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot()
         val projects: List[IProject] = workspaceRoot.getProjects().toList
         projects.filter{x => x.isOpen() && x.hasNature(JavaCore.NATURE_ID)}
         //select all java projects in workspace
         
         for (project: IProject <- projects
                 if result == null) {
              var jProject: IJavaProject = JavaCore.create(project)
              
              if(jProject.getHandleIdentifier.equals(activeProject)) {
                  //select the active project
                  result = findClassInJars(jProject, name) //try to find in jars
                  if(result == null) {
                      result = findClassInSource(jProject, name) //try to find in output folder
                  }
              }
         }
         result
    }
}