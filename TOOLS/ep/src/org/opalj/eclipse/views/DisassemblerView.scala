/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2015
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.opalj.eclipse.views

import org.eclipse.swt.SWT
import org.eclipse.swt.browser.Browser
import org.eclipse.swt.browser.LocationEvent
import org.eclipse.swt.browser.LocationListener
import org.eclipse.swt.browser.ProgressEvent
import org.eclipse.swt.browser.ProgressListener
import org.eclipse.swt.widgets.Composite
import org.eclipse.ui.part.ViewPart
import org.opalj.da.Constant_Pool_Entry
import org.eclipse.swt.layout.RowData
import scala.collection.mutable.Stack
import org.eclipse.swt.widgets.Button
import org.opalj.da.ClassFile
import org.opalj.eclipse.Activator
import org.eclipse.swt.widgets.Listener
import org.eclipse.swt.widgets.Event
import org.opalj.eclipse.FindBUnit
import java.io.FileWriter
import java.io.BufferedWriter
import java.io.File
import jdk.internal.org.objectweb.asm.tree.MethodNode
import org.eclipse.jface.action.Action
import org.eclipse.jface.resource.ImageDescriptor
import java.net.URL
import java.net.MalformedURLException
import org.eclipse.jface.action.IToolBarManager

object DisassemblerView {
    val Id: String = "org.opalj.eclipse.views.DisassemblerView"
    val classNameToLinkJS: String =
        """document.querySelectorAll(".fqn").forEach(function(e) {
        |  const title = e.getAttribute("title");
        |  // delete leading whitespace and periods, also delete trailing square bracket
        |  const text = e.innerHTML.replace(/^[.,\s]+/gm,'').replace(/[\[\]]+$/gm,'');
        |  const parent = e.parentNode;
        |  var fqn;
        |  if (e.getAttribute("title") != null) {
        |    fqn = e.getAttribute("title") + text;
        |  } else {
        |    fqn = text;
        |  }
        |
        |  // skip primitive types
        |  const primitives = ['void', 'byte', 'short', 'int', 'long',
        |                      'float', 'double', 'boolean', 'char'];
        |  if(primitives.indexOf(fqn) !== -1) return;
        |
        |  var link = document.createElement("a");
        |  link.setAttribute("href", "daRedirect-" + fqn);
        |  link.innerHTML = e.innerHTML
        |  parent.replaceChild(link, e);
        |});""".stripMargin;
    val methodNameToLinkJS: String =
        """document.querySelectorAll(".method_name").forEach(function(e) {
        |  var methodName = e.getAttribute('methodName');
        |  var signature = e.getAttribute('signature');
        |  var className;
        |  
        |  var iter = e.parentElement;
        |  while(iter.getAttribute('class') != 'cp_string'){
        |    iter = iter.parentElement;
        |  }
        |  
        |  var classLink = iter.getElementsByTagName('a')[0];
        |  var className = classLink.getAttribute('href')
        |  
        |  e.innerHTML = '';
        |  
        |  var link = document.createElement("a");
        |  link.setAttribute("href", className+'#'+methodName+signature);
        |  link.innerHTML = methodName;
        |  e.appendChild(link);
        |});""".stripMargin;
}

/**
 * Eclipse view for displaying class file disassembly
 * @author Lukas Becker
 * @author Simon Bohlender
 * @author Simon Guendling
 * @author Felix Zoeller
 */
class DisassemblerView extends ViewPart {

    var browser: Browser = _
    var activePojectId: String = null //project id of selected unit for disassembling
    var currentClass: String = null
    var backAction: Action = null
    var forwardAction: Action = null
    val backStack = new Stack[String]
    val forwardStack = new Stack[String]
    var methodRedirect: Boolean = false
    var methodName: String = null

    def createPartControl(parent: Composite): Unit = {
        //create layout for the view
        browser = new Browser(parent, SWT.NONE)
        browser.addProgressListener(new ProgressListener() {
            def changed(x$1: ProgressEvent): Unit = {}

            def completed(x$1: ProgressEvent): Unit = {
                browser.execute(DisassemblerView.classNameToLinkJS)
                browser.execute(DisassemblerView.methodNameToLinkJS)
                if(methodRedirect) {
                    browser.execute("location.hash = '" + methodName + "'")
                    methodRedirect = false
                    methodName = null
                }
            }
        })
        browser.addLocationListener(new LocationListener() {
            def changed(l: LocationEvent): Unit = {
            }
            def changing(l: LocationEvent): Unit = {
                if (l.location.startsWith("file://")) {
                    l.doit = false
                } else {
                    classClicked(l.location.split(":")(1))
                }
            }
        })
        createActions
        createToolbar
    }
    
    def createActions(): Unit = {
        backAction = new Action("Back...") {
            override def run(): Unit = { 
                backClicked()
            }
        }
        backAction.setImageDescriptor(getImageDescriptor("left-arrow.png"))
        backAction.setEnabled(false)
        
        forwardAction = new Action("Forward") {
            override def run(): Unit = { 
                forwardClicked()
            }
        }
        forwardAction.setImageDescriptor(getImageDescriptor("right-arrow.png"))
        forwardAction.setEnabled(false)
    }
    
    /**
    * Returns the image descriptor with the given relative path.
    */
    def getImageDescriptor(relativePath: String): ImageDescriptor = {
        val iconPath = "icons/";
        try {
            val plugin: Activator = Activator.getDefault()
            val installURL: URL = plugin.getDescriptor().getInstallURL();
            val url: URL = new URL(installURL, iconPath + relativePath);
            return ImageDescriptor.createFromURL(url);
        } catch {
            // should not happen
            case e: MalformedURLException => return ImageDescriptor.getMissingImageDescriptor();
        }
    }
    
    def createToolbar(): Unit = {
        val mgr: IToolBarManager = getViewSite().getActionBars().getToolBarManager();
        mgr.add(backAction);
        mgr.add(forwardAction);
    }

    def setFocus(): Unit = {
        browser.setFocus
    }

    def setText(text: String): Unit = {
        browser.setText(text)

    }

    override def setPartName(text: String): Unit = {
        super.setPartName(text)
    }
    
    def setActivePojectId(activePojectId: String): Unit = {
        this.activePojectId = activePojectId
    }
    
    def backClicked(): Unit = {
        forwardStack.push(currentClass)
        val name = backStack.pop
        currentClass = name
        forwardAction.setEnabled(true)
        updateBrowser(name)
        if(backStack.isEmpty) {
            backAction.setEnabled(false)
        }
    }
    
    def forwardClicked(): Unit = {
        backStack.push(currentClass)
        val name = forwardStack.pop
        currentClass = name
        backAction.setEnabled(true)
        updateBrowser(name)
        if(forwardStack.isEmpty) {
            forwardAction.setEnabled(false)
        }
    }
    
    def classClicked(name: String): Unit = {
        val prefix = "daRedirect-"
        if (name.startsWith(prefix)) {
            backStack.push(currentClass)
            forwardStack.clear
            backAction.setEnabled(true)
            forwardAction.setEnabled(false)
            val fqn = name.substring(prefix.length)
            currentClass = fqn
            updateBrowser(fqn)
        }
    }
    
    def updateBrowser(name: String): Unit = {
        var className: String = null
        if(name.indexOf("#") > 0){
            className = name.substring(0, name.indexOf("#"))
            methodName = name.substring(name.indexOf("#") + 1)
            methodRedirect = true
        } else{
            className = name
        }
        //find class in project
        val classObj: ClassFile = FindBUnit.unitLookup(className, activePojectId)
        updateHTML(classObj)
    }
    
    def initBrowser(classObj: ClassFile): Unit = {
        currentClass = classObj.fqn
        updateHTML(classObj)
    }
    
    def updateHTML(classObj: ClassFile): Unit = {
        val file = classOf[Activator].getResourceAsStream("style.css")
        val css = scala.io.Source.fromInputStream(file).mkString
        val html: String = classObj.toXHTML(css).toString
        
        val cpe: Constant_Pool_Entry = classObj.constant_pool(classObj.this_class)
        setPartName(cpe.toString(classObj.constant_pool).split('.').last)
        setText(html)
    }
}