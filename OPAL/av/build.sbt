name := "Architecture Validation"

//version := "0.2.0" // LAST RELEASE
version := "0.9.0-SNAPSHOT"

scalacOptions in (Compile, doc) := Opts.doc.title("OPAL - Architecture Validation") 